from django.contrib import admin

from django.urls import path, include
from django.views.generic.base import TemplateView

from rest_framework.schemas import get_schema_view

urlpatterns = [

    path('admin/', admin.site.urls),
    path('', include('productos.urls')),
    path('openapi/', get_schema_view(
        title="Productos Service",
        description="API developers hpoing to use our service"
    ), name='openapi-schema'),
    path('docs/', TemplateView.as_view(
        template_name='documentation.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),

]
