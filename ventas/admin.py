from django.contrib import admin

from ventas.models import MethodOfPaymentSell


# Register your models here.
@admin.register(MethodOfPaymentSell)
class MethodOfPaymentSellAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')
    search_fields = ('description',)

