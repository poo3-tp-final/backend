from django.db import models

from clientes.models import Client
from compras.models import Subsidiary
from productos.models import Product


class MethodOfPaymentSell(models.Model):
    description = models.CharField(max_length=255)

    class Meta:
        ordering = ['id']
        verbose_name = 'Método de Pago venta'
        verbose_name_plural = "Métodos de Pago ventas"

    def __str__(self):
        return self.description


class Sell(models.Model):
    sold = models.DateTimeField(auto_now_add=True)
    subsidiary = models.ForeignKey(Subsidiary, on_delete=models.PROTECT)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    send = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'Venta'
        verbose_name_plural = "Ventas"

    def __str__(self):
        return f" ${self.id} - ${self.client.name} - ${self.subsidiary.description} "


class SellDetail(models.Model):
    sell = models.ForeignKey(Sell, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    method_of_payment = models.ForeignKey(MethodOfPaymentSell, on_delete=models.PROTECT)
    sell_amount = models.DecimalField("importe_venta", max_digits=12, decimal_places=2)
    profit = models.DecimalField("margen", max_digits=12, decimal_places=2)
    gross_profit = models.DecimalField("margen_bruto", max_digits=12, decimal_places=2)
    quantity = models.IntegerField()

    class Meta:
        ordering = ['id']
        verbose_name = 'Venta Detalle'
        verbose_name_plural = "Ventas detalles"

    def __str__(self):
        return self.id


class PriceList(models.Model):
    description = models.CharField(max_length=255)
    from_date = models.DateTimeField(blank=True, null=True)
    to_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'Lista de precios'
        verbose_name_plural = "Listas de Precios"

    def __str__(self):
        return self.description


class PriceListDetail(models.Model):
    price_list = models.ForeignKey(PriceList, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    price = models.DecimalField("precio", max_digits=12, decimal_places=2)

    class Meta:
        ordering = ['id']
        verbose_name = 'Lista de precios detalle'
        verbose_name_plural = "Listas de Precios detalles"

    def __str__(self):
        return f"${self.product.description} - ${self.price}"
