from django.contrib import admin

from clientes.models import Client,  AcquisitionChannel


# Register your models here.
@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'last_name', 'dni', 'phone', 'email', 'address', 'postal_code',
                    'cuit_cuil', 'AcquisitionChannel',)
    search_fields = ('name', 'last_name', 'dni',)
    list_filter = ('postal_code',)


@admin.register(AcquisitionChannel)
class AcquisitionChannelAdmin(admin.ModelAdmin):
    list_display = ('id', 'description',)
    search_fields = ('description',)
