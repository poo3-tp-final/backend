from django.db import models


# Create your models here.

class AcquisitionChannel(models.Model):
    description = models.CharField(max_length=255, verbose_name='Fuente')

    class Meta:
        verbose_name = 'Canal de Conocimiento'
        verbose_name_plural = "Canales de Conocimiento"


class Client(models.Model):
    name = models.CharField(max_length=255, verbose_name='nombre')
    last_name = models.CharField(max_length=255, verbose_name='apellido')
    dni = models.CharField(max_length=100, verbose_name='dni')
    phone = models.CharField(max_length=100, verbose_name='teléfono')
    email = models.EmailField(max_length=70, null=True, blank=True)
    address = models.CharField(max_length=100, verbose_name='dirección')
    postal_code = models.CharField(max_length=100, verbose_name='Código postal')
    cuit_cuil = models.CharField(max_length=100, verbose_name='Cuit_cuil', null=True, blank=True)
    AcquisitionChannel = models.ForeignKey(AcquisitionChannel, on_delete=models.CASCADE, related_name='canal',
                                           verbose_name='Canal',  null=True, blank=True)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = "Clientes"
