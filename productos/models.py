from django.db import models


# Create your models here.

class Category(models.Model):
    description = models.CharField(max_length=255)

    class Meta:
        ordering = ['id']
        verbose_name = 'Categoría'

    def __str__(self):
        return f"{self.description}"


class Type(models.Model):
    description = models.CharField(max_length=255)

    class Meta:
        ordering = ['id']
        verbose_name = 'Tipo'

    def __str__(self):
        return f"{self.description}"


class Brand(models.Model):
    description = models.CharField(max_length=255)

    class Meta:
        ordering = ['id']
        verbose_name = 'Marca'

    def __str__(self):
        return f"{self.description}"


class Supplier(models.Model):
    description = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)

    class Meta:
        ordering = ['id']
        verbose_name = 'Proveedor'
        verbose_name_plural = "Proveedores"

    def __str__(self):
        return f"{self.description}"


class Product(models.Model):
    description = models.CharField(max_length=255)
    type = models.ForeignKey(Type, on_delete=models.PROTECT)
    brand = models.ForeignKey(Brand, on_delete=models.PROTECT, blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    created = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    supplier_code = models.CharField(max_length=255)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'Producto'
        verbose_name_plural = "Productos"

    def __str__(self):
        return f"{self.description}"
