from django.contrib import admin

from productos.models import Type, Category, Product, Brand, Supplier


class TypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')


class BrandAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')


class SupplierAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'description', 'type', 'brand', 'category', 'created',
                    'last_update', 'supplier_code', 'active')
    search_fields = ('id', 'description')


admin.site.register(Type, TypeAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Supplier, SupplierAdmin)
