from django.urls import path
from rest_framework import routers

from . import views
from .views import ProductsListAPIView, ProductsRetrieveUpdateDestroytAPIView, BrandListAPIView, CategoryListAPIView, \
    TypeListAPIView, ProductCreateAPIView

route = routers.SimpleRouter()

urlpatterns = route.urls

urlpatterns += [path('login', views.CustomAuthToken.as_view()),
                path('products', ProductsListAPIView.as_view()),
                path('products/<str:pk>', ProductsRetrieveUpdateDestroytAPIView.as_view()),
                path('brands', BrandListAPIView.as_view()),
                path('categories', CategoryListAPIView.as_view()),
                path('types', TypeListAPIView.as_view()),
                path('product/create', ProductCreateAPIView.as_view()),



                ]
