# importamos el modelo user propio de django
from datetime import timedelta

from django.contrib.auth.models import User
from rest_framework import generics, status
from rest_framework.authtoken.views import ObtainAuthToken
# decorador para decirle a django que nuestra vista es un apiview
from rest_framework.decorators import api_view
# importamos la clase Token
from rest_framework.authtoken.models import Token
# Funcion que comprueba el hash del password
from django.contrib.auth.hashers import check_password
from rest_framework.permissions import IsAuthenticated
# importamos clase Response para generar respuestas validas
from rest_framework.response import Response

from productos.models import Product, Category, Type, Brand
from productos.serializers import ProductoSerializers, TipoSerializers, CategoriaSerializers, BrandSerializer, \
    ProductCreateSerializer


@api_view(['POST'])
def login(request):
    # obtenemos parametros desde el objeto request
    username = request.POST.get('username')
    password = request.POST.get('password')
    # buscamos el usuario
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return Response('Usuario invalido')

    # verificamos el hash con la password obtenida,devuelve true o false
    pwd_valid = check_password(password, user.password)
    # si es falso cortamos ejecucion
    if not pwd_valid:
        return Response('Password invalida')
    # si es verdadero creaamos el token y lo devolvemos, get_or_create devuelve una tupla
    token, _ = Token.objects.get_or_create(user=user)

    return Response(token.key)


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email,
            'expires_in': token.created + timedelta(hours=24)
        })


class ProductsListAPIView(generics.ListCreateAPIView):
    serializer_class = ProductoSerializers

    def get_queryset(self):
        return Product.objects.all()


class ProductsRetrieveUpdateDestroytAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ProductoSerializers

    def get_queryset(self):
        return Product.objects.filter(id=self.kwargs["pk"])

    def update(self, request, *args, **kwargs):
        product = request.data
        product_to_update = Product.objects.filter(id=self.kwargs["pk"]).first()
        product_to_update.description = product['description']
        product_to_update.brand = Brand.objects.filter(id=product['brand_id']).first()
        product_to_update.category = Category.objects.filter(id=product['category_id']).first()
        product_to_update.type = Type.objects.filter(id=product['type_id']).first()
        product_to_update.supplier_code = product['supplier_code']
        product_to_update.save()
        return Response('ok', status=status.HTTP_200_OK)


class ProductCreateAPIView(generics.CreateAPIView):
    queryset = Product.objects.all()
    serializer = ProductCreateSerializer

    def create(self, request, *args, **kwargs):
        product = request.data
        product_created = Product.objects.create(
            description=product['description'],
            brand=Brand.objects.filter(id=product['brand_id']).first(),
            category=Category.objects.filter(id=product['category_id']).first(),
            type=Type.objects.filter(id=product['type_id']).first(),
            supplier_code=product['supplier_code']
        )
        return product_created


class CategoryListAPIView(generics.ListCreateAPIView):
    serializer_class = CategoriaSerializers

    def get_queryset(self):
        return Category.objects.all()


class TypeListAPIView(generics.ListCreateAPIView):
    serializer_class = TipoSerializers

    def get_queryset(self):
        return Type.objects.all()


class BrandListAPIView(generics.ListCreateAPIView):
    serializer_class = BrandSerializer

    def get_queryset(self):
        return Brand.objects.all()
