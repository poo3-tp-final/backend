from rest_framework import serializers

from productos.models import Category, Product, Type, Brand


class CategoriaSerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'description']


class TipoSerializers(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = '__all__'

class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class ProductoSerializers(serializers.ModelSerializer):
    category = serializers.SerializerMethodField(read_only=True)
    category_id = serializers.SerializerMethodField(read_only=True)
    type = serializers.SerializerMethodField(read_only=True)
    type_id = serializers.SerializerMethodField(read_only=True)
    brand = serializers.SerializerMethodField(read_only=True)
    brand_id = serializers.SerializerMethodField(read_only=True)
    created = serializers.SerializerMethodField(read_only=True)
    active = serializers.SerializerMethodField(read_only=True)
    active_value = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'description', 'created', 'type', 'brand', 'category', 'type_id', 'brand_id', 'category_id',
                  'supplier_code', 'active', 'active_value']

    def get_category(self, obj):
        return obj.category.description

    def get_type(self, obj):
        return obj.type.description

    def get_brand(self, obj):
        return obj.brand.description

    def get_category_id(self, obj):
        return obj.category.id

    def get_type_id(self, obj):
        return obj.type.id

    def get_brand_id(self, obj):
        return obj.brand.id

    def get_created(self, obj):
        return obj.created.time()

    def get_active(self, obj):
        return 'si' if obj.active is True else 'no'

    def get_active_value(self, obj):
        return obj.active


class ProductCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['__all__']

