# Generated by Django 4.0 on 2022-11-11 00:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('productos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Supplier',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=255)),
                ('address', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'Proveedor',
                'ordering': ['id'],
            },
        ),
        migrations.AlterModelOptions(
            name='brand',
            options={'ordering': ['id'], 'verbose_name': 'Marca'},
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['id'], 'verbose_name': 'Categoría'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['id'], 'verbose_name': 'Producto'},
        ),
        migrations.AlterModelOptions(
            name='type',
            options={'ordering': ['id'], 'verbose_name': 'Tipo'},
        ),
        migrations.RenameField(
            model_name='product',
            old_name='categoria',
            new_name='category',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='creacion',
            new_name='created',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='ultima_actualizacion',
            new_name='last_update',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='titulo',
            new_name='supplier_code',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='tipo',
            new_name='type',
        ),
        migrations.RemoveField(
            model_name='product',
            name='precio_costo',
        ),
        migrations.RemoveField(
            model_name='product',
            name='url_clean',
        ),
        migrations.AddField(
            model_name='product',
            name='active',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='product',
            name='brand',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='productos.brand'),
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.CharField(max_length=255),
        ),
    ]
