from django.contrib import admin
import nested_admin

from compras.models import BuyStatus, MethodOfPaymentBuy, Subsidiary, BuyDetail, Buy


# Register your models here.

@admin.register(BuyStatus)
class BuyStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'description', 'color')
    search_fields = ('description',)


@admin.register(MethodOfPaymentBuy)
class MethodOfPaymentBuyAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')
    search_fields = ('description',)


@admin.register(Subsidiary)
class SubsidiaryAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')
    search_fields = ('description',)


##class BuyDetailInline(nested_admin.NestedStackedInline):
  ##  model = BuyDetail
    ##extra = 0


##@admin.register(Buy)
#class BuyAdmin(nested_admin.NestedModelAdmin):
 #   list_display = ('id', 'supplier', 'bought', 'bill_number', 'method_of_payment', 'total_amount', 'buy_status',
  #                  'active',)
   # search_fields = ('supplier', 'bill_number',)
    #list_filter = ('supplier', 'active',)
#
 #   inlines = [BuyDetailInline]

