from django.db import models

from productos.models import Supplier, Product


class BuyStatus(models.Model):
    description = models.CharField(max_length=255)
    color = models.CharField(max_length=255)

    class Meta:
        ordering = ['id']
        verbose_name = 'Estado de compra'

    def __str__(self):
        return f"{self.description}"


class MethodOfPaymentBuy(models.Model):
    description = models.CharField(max_length=255)

    class Meta:
        ordering = ['id']
        verbose_name = 'Método de Pago compra'
        verbose_name_plural = "Métodos de Pago compras"

    def __str__(self):
        return f"{self.description}"


class Subsidiary(models.Model):
    description = models.CharField(max_length=255)

    class Meta:
        ordering = ['id']
        verbose_name = 'Sucursal'
        verbose_name_plural = "Sucursales"

    def __str__(self):
        return f"{self.description}"


class Buy(models.Model):
    supplier = models.ForeignKey(Supplier, on_delete=models.PROTECT)
    bought = models.DateTimeField(auto_now_add=True)
    bill_number = models.CharField(max_length=255)
    method_of_payment = models.ForeignKey(MethodOfPaymentBuy, on_delete=models.PROTECT)
    total_amount = models.DecimalField("importe", max_digits=12, decimal_places=2)
    buy_status = models.ForeignKey(BuyStatus, on_delete=models.PROTECT)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'Compra'
        verbose_name_plural = "Compras"

    def __str__(self):
        return f"{self.id}"


class BuyDetail(models.Model):
    buy = models.ForeignKey(Buy, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    received = models.DateTimeField(auto_now_add=True)
    subsidiary = models.ForeignKey(Subsidiary, on_delete=models.PROTECT)
    amount = models.DecimalField("importe", max_digits=12, decimal_places=2)

    class Meta:
        ordering = ['id']
        verbose_name = 'Detalle Compra'
        verbose_name_plural = "Detalles Compras"

    def __str__(self):
        return f"{self.description}"


