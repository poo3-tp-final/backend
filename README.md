# Base Django Rest Framework API(DRF)
Proyecto Tienda mas carga de productos con autentificacion por token JWT.



### Requirements

This projects requires python 3.9.15
Python 3 can be installed with [pyenv](https://github.com/pyenv/pyenv).

1. Use [pyenv-installer](https://github.com/pyenv/pyenv-installer) for installing pyenv
1. See which python versions are available: `pyenv install --list`
1. Install python 3. Example: `pyenv install 3.6.3` (3.6.3 or higher)

## Local setUp

1. Create a new virtualenv with a meaningful name: `pyenv virtualenv 3.9.15 my_virtualenv`
1. Create a `.python-version` file: `echo "my_virtualenv" > .python-version`
2. Install the requirements: `pip install -r requirements.txt`

## Run server

* `./manage.py runserver`

## Run shell

* `./manage.py shell`

## Run tests

* `python manage.py test`



## Desarrollo:

| Contenido | Detalle |
| ------ | ------ |
| Django | 4.0 |
| django-rest-swagger | 2.2.0 |
| djangorestframework | 3.13.1 |
| openapi-codec | 1.3.2 |
| PyYAML | 6.0 |


<details>
<summary>
Bases de Datos
</summary>

| Base de Datos | Usuarios | password |
| ------ | ------ | ------ | 
| sqlite3 | maxi1234 | 123456 | 


</details>



# 

### *Ej. Creacion de un nuevo ISSUE *



- [x] Previamente se debe ingresar con a [GITLAB](https://gitlab.com/)


    > Buenas Tardes,
    > Se solicia:
    > 
    > 1) Pequeña descripción de la solicitud.
    > 
    > 2) Copiar los logs  y adjuntarlos por este medio.
    > 
    > 3) Aviso al staff por los diferentes canales provistos
    > 
    > A los canales:
    >  - Gitlab issues
    > 
    > 
    > Muchas gracias!

# 
